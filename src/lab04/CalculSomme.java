/*
 * @ Author: Johnny Tsheke @ UQAM
 * 2017-02-07
 */
package lab04;
import java.util.*;
public class CalculSomme {
    static final int ENTIER_MAX = 1000;
    
	public static void main(String[] args) {
	  final String MESSAGE_BIENVENU ="Bienvenu";
	  final String MESSAGE_CE_QUE_FAIT_PROGRAMME ="Ce programme calcule la somme de n premier nombre entiers";
      final String MESSAGE_FIN = "Fin du programme";
	  Scanner clavier = new Scanner(System.in);
      
      int nombre = 0;
      System.out.println(MESSAGE_BIENVENU);
      System.out.println(MESSAGE_CE_QUE_FAIT_PROGRAMME);
      System.out.format("Entrez un nombre entier inférieur ou égal à %d pour faire le calcul %n",ENTIER_MAX);
      if(clavier.hasNextInt()){
    	  int nombreTemporaire = clavier.nextInt();
    	  if(nombreTemporaire <= ENTIER_MAX){
    		  nombre = nombreTemporaire;
    		  int somme = 0;
    		  for (int i=0;i<= nombre; i++){
    			  somme = somme + i;
    		  }
    		  System.out.format("La somme de %d premiers nombre entiers est éégale à %d %n",nombre,somme);
    	  }
      }
      System.out.println(MESSAGE_FIN);
      clavier.close();
    
	}

}
