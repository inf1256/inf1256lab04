/*
 * @ Author: Johnny Tsheke @ UQAM
 * 2017-02-07
 */
package lab04;
import java.util.*;
public class CaisseSimple {
	 
	public static void main(String[] args) {
		final String MESSAGE_BIENVENU ="Bienvenu";
	 final String MESSAGE_CE_QUE_FAIT_PROGRAMME ="Ce programme calcule la somme d'une caisse enregistreuse. Il s'arrete normalement si on tape le mot FIN";
	    final String MESSAGE_FIN = "Fin du programme";
		
		System.out.println(MESSAGE_BIENVENU);
	     System.out.println(MESSAGE_CE_QUE_FAIT_PROGRAMME);
	     Scanner clavier = new Scanner(System.in).useLocale(Locale.US);
	     boolean continuer = true;
	     String patterneFin="FIN";
	     double somme = 0.0;
	     while(continuer){
	    	 System.out.println("Prix de l'article suivant svp");
	    	 if(clavier.hasNext(patterneFin)){
	    		 continuer = false;
	    	 }else if(clavier.hasNextDouble()){
	    		 somme = somme + clavier.nextDouble();
	    	 }else{
	    		 System.out.println("Erreur!");
	    		 break;
	    	 }
	     }
	     System.out.format("Le montant total est %.2f %n",somme);
	     System.out.println(MESSAGE_FIN);
	     clavier.close();
	}

}
